package org.example

class MethodsContainer {

    private val joints = listOf(
        listOf(0.0, -1.5707963267948966, 0.0, -1.5707963267948966, 0.0, 0.0),
        listOf(0.0, 0.0, 0.0, 1.5707963267948966, 1.5707963267948966, 0.0),
        listOf(0.0, 0.0, 0.0, 0.0, 0.0, 0.0))

    fun randomJoint(): List<Double> {
        return joints.random()
    }
}