package org.example

import org.apache.xmlrpc.server.PropertyHandlerMapping
import org.apache.xmlrpc.webserver.WebServer

class XmlRpcServer {

    private val server = WebServer(12000)
    private val xmlRpcServer = server.xmlRpcServer
    private val mapping = PropertyHandlerMapping()

    init {
        mapping.addHandler("get", MethodsContainer::class.java)
        xmlRpcServer.handlerMapping = mapping
    }

    fun start() {
        server.start()
    }

    fun stop() {
        server.shutdown()
    }
}